import { Component, OnInit } from '@angular/core';
import { FacesnaplistComponent } from './facesnaplist/facesnaplist.component';
import { HeaderComponent } from './header/header.component';
import { RouterOutlet } from '@angular/router';
import { FormBuilder,FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [FacesnaplistComponent,HeaderComponent,RouterOutlet,ReactiveFormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  constructor(
    formBuilder:FormBuilder
  ) {

  }
  ngOnInit() {}
}
