import { Component, OnInit } from '@angular/core';
import { Router, RouterLink, RouterLinkActive } from '@angular/router';
import {FormsModule, NgForm} from '@angular/forms';

@Component({
  selector: 'app-landing-page',
  standalone: true,
  imports: [RouterLink,FormsModule],
  templateUrl: './landing-page.component.html',
  styleUrl: './landing-page.component.scss'
})
export class LandingPageComponent implements OnInit{
  userEmail!: string;

  constructor(private router:Router){}
    onclickButton():void{
      this.router.navigateByUrl("facesnaps");
    }

    ngOnInit(): void {
        
    }
  
    onSubmitForm(form:NgForm) {
      console.log(form.value);
    }
}
