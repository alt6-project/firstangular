import { Component, Input, OnInit} from '@angular/core';
import { FaceSnap } from '../models/face-snap';
import { DatePipe, LowerCasePipe, NgClass, NgStyle, TitleCasePipe, UpperCasePipe } from '@angular/common';
import { FaceSnapService } from '../services/face-snaps.service';
import { ActivatedRoute, RouterLink, } from '@angular/router';

@Component({
  selector: 'app-single-facesnap',
  standalone: true,
  imports: [NgStyle,NgClass,DatePipe,TitleCasePipe,RouterLink],
  templateUrl:'./single-facesnap.component.html',
  styleUrl: './single-facesnap.component.scss'
})
export class SingleFacesnapComponent implements OnInit {
  faceSnap!: FaceSnap;
  snapButtonText!: string;
  userHasSnapped!: boolean;

  constructor(private faceSnapsService: FaceSnapService,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.preparingButton();
    this.getUserSnap();
  }
  onAddSnap():void{
    if (this.userHasSnapped) {
      this.snap();
    } else {
      this.unSnap();
    }
  }

  snap():void{
    this.faceSnapsService.snapFaceBySnapId(this.faceSnap.id,'snap');
    this.snapButtonText = 'Oh Snap!';
    this.userHasSnapped = false;
  }

  unSnap():void{
    this.faceSnapsService.snapFaceBySnapId(this.faceSnap.id,'unsnap');
    this.snapButtonText = 'Oops, unSnap!';
    this.userHasSnapped = true;
  }

  private preparingButton(){
    this.snapButtonText="Oh Snap";
    this.userHasSnapped=false
  }
  private getUserSnap(){
    const snapshotId = this.route.snapshot.params['id'];
    this.faceSnap = this.faceSnapsService.getFaceSnapById(snapshotId);
  }

}
