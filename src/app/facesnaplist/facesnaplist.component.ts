import { Component, OnInit } from '@angular/core';
import { FaceSnap } from '../models/face-snap';
import { FacesnapComponent } from '../facesnap/facesnap.component';
import { FaceSnapService } from '../services/face-snaps.service';
import { Observable } from 'rxjs';
import { AsyncPipe, NgFor } from '@angular/common';


@Component({
  selector: 'app-facesnaplist',
  standalone: true,
  imports: [FacesnapComponent,AsyncPipe,NgFor],
  templateUrl: './facesnaplist.component.html',
  styleUrl: './facesnaplist.component.scss'
})
export class FacesnaplistComponent implements OnInit {
  faceSnaps?:FaceSnap[];
  faceSnaps$?: Observable<FaceSnap[]>;
  constructor(private faceSnapService:FaceSnapService){
    
  }
  ngOnInit(): void {
     // this.faceSnaps = this.faceSnapService.getFaceSnaps();
    this.faceSnaps$ = this.faceSnapService.getFaceSnaps();
  }

}
