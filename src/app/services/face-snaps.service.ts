import { Injectable } from "@angular/core";
import { FaceSnap } from "../models/face-snap";
import { SnapType } from "./snap-types.type";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable({
    providedIn:'root'
})
export class FaceSnapService{
    private url = "";
    constructor(private http:HttpClient){
    }
    facesnaps:FaceSnap[] = [
        new FaceSnap(
        "test",
        "testdescription",
        new Date(),
        140, 
        'https://cdn.pixabay.com/photo/2015/05/31/16/03/teddy-bear-792273_1280.jpg'
        ),
        new FaceSnap(
        "test2",
        "testdescription2",
        new Date(),
        16, 
        'https://cdn.pixabay.com/photo/2015/05/31/16/03/teddy-bear-792273_1280.jpg'
        ).withLocation("à la montagne"),
        new FaceSnap(
        "test3",
        "testdescription3",
        new Date(),
        3, 
        'https://cdn.pixabay.com/photo/2015/05/31/16/03/teddy-bear-792273_1280.jpg'
        )
    ]
    getFaceSnaps():Observable<FaceSnap[]>{
        return this.http.get<FaceSnap[]>('http://localhost:3000/facesnaps');
    }

    getFaceSnapById(snapFaceId:string):FaceSnap{
        const foundSnapFace = this.facesnaps.find((item)=>item.id===snapFaceId);
        if(!foundSnapFace){
            throw new Error("notFound");
        }
        return foundSnapFace;
    }

    snapFaceBySnapId(snapFaceId:string,snapType:SnapType):void{
        const foundSnapFace = this.getFaceSnapById(snapFaceId);
        foundSnapFace.snap(snapType); 
    }

    addFaceSnap(formValue: { title: string, description: string, imageUrl: string, location?: string }):void{
    const r = {
        ...formValue,
        snaps: 0,
        createdAt: new Date(),
        id: this.facesnaps[this.facesnaps.length - 1].id + 1
    };
    this.facesnaps.push(r as FaceSnap);
}

}
