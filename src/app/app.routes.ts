import { Routes } from '@angular/router';
import { FacesnaplistComponent } from './facesnaplist/facesnaplist.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { SingleFacesnapComponent } from './single-facesnap/single-facesnap.component';
import { NewFaceSnapComponent } from './new-face-snap/new-face-snap.component';

export const routes: Routes = [ 
    { path: 'facesnaps/:id', component:SingleFacesnapComponent},
    { path: '', component:LandingPageComponent},
    { path: 'facesnaps', component:FacesnaplistComponent},
    { path: 'create', component: NewFaceSnapComponent },
];
