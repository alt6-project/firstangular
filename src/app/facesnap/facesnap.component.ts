import { Component, Input, OnInit} from '@angular/core';
import { FaceSnap } from '../models/face-snap';
import { DatePipe, LowerCasePipe, NgClass, NgStyle, TitleCasePipe, UpperCasePipe } from '@angular/common';
import { FaceSnapService } from '../services/face-snaps.service';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-facesnap',
  standalone: true,
  imports: [NgStyle,NgClass,LowerCasePipe,UpperCasePipe,TitleCasePipe,DatePipe],
  templateUrl: './facesnap.component.html',
  styleUrl: './facesnap.component.scss'
})
export class FacesnapComponent{
  @Input() faceSnap!: FaceSnap;

  constructor(private router: Router) {}

  onViewFaceSnap() {
    this.router.navigateByUrl(`facesnaps/${this.faceSnap.id}`);
  }

}
